<?php
/**
 * parameters_prod.php.
 */
$db = parse_url(getenv('CLEARDB_DATABASE_URL'));

$container->setParameter('database_driver', 'pdo_mysql');
$container->setParameter('database_host', $db['host']);
$container->setParameter('database_port', $db['port']);
$container->setParameter('database_name', substr($db['path'], 1));
$container->setParameter('database_user', $db['user']);
$container->setParameter('database_password', $db['pass']);
$container->setParameter('database_key', getenv('CLEARDB_DATABASE_KEY'));
$container->setParameter('database_cert', getenv('CLEARDB_DATABASE_CERT'));
$container->setParameter('database_ca', getenv('CLEARDB_DATABASE_CA'));
$container->setParameter('secret', getenv('SECRET'));
$container->setParameter('locale', 'en');
$container->setParameter('mailer_transport', 'smtp');
$container->setParameter('mailer_host', 'smtp.sendgrid.net');
$container->setParameter('mailer_user', getenv('SENDGRID_USERNAME'));
$container->setParameter('mailer_password', getenv('SENDGRID_PASSWORD'));
$container->setParameter('mailer_port', '587');
$container->setParameter('sf_username', getenv('SF_USERNAME'));
$container->setParameter('sf_password', getenv('SF_PASSWORD'));
$container->setParameter('sf_token', getenv('SF_TOKEN'));
$container->setParameter('sf_api_url', getenv('SF_API_URL'));
$container->setParameter('sf_consumer_key', getenv('SF_CONSUMER_KEY'));
$container->setParameter('sf_consumer_secret', getenv('SF_CONSUMER_SECRET'));
$container->setParameter('sf_callback_url', 'https://vcchcportal.com/salesforce/callback');
$container->setParameter('sf_veteran_status', getenv('SF_VETERAN_STATUS'));
$container->setParameter('sf_couple_status', getenv('SF_COUPLE_STATUS'));
$container->setParameter('sf_spouse_status', getenv('SF_SPOUSE_STATUS'));
$container->setParameter('referral_email_notification', getenv('REFERRAL_EMAIL'));
$container->setParameter('admin_email_notification', getenv('ADMIN_EMAIL'));
$container->setParameter('admin_email_superuser', 'VCC Admin');
$container->setParameter('veteran_va_benefit', getenv('VETERAN_VA_BENEFIT'));
$container->setParameter('spouse_va_benefit', getenv('SPOUSE_VA_BENEFIT'));
$container->setParameter('couple_va_benefit', getenv('COUPLE_VA_BENEFIT'));
$container->setParameter('assets_limit_calculation', getenv('ASSETS_LIMIT_CALCULATION'));
